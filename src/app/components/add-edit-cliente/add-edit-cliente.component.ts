import { Component, OnInit } from '@angular/core';
import { MAT_RADIO_DEFAULT_OPTIONS } from '@angular/material/radio';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Emprendedor } from 'src/app/models/emprendedor';
import { ClienteService } from 'src/app/services/cliente.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-add-edit-cliente',
  templateUrl: './add-edit-cliente.component.html',
  styleUrls: ['./add-edit-cliente.component.css'],
  providers: [{
    provide: MAT_RADIO_DEFAULT_OPTIONS,
    useValue: { color: 'primary' },
  }]
})
export class AddEditClienteComponent implements OnInit {
    id_cliente: any;
  myForm: FormGroup;
  constructor(private fb: FormBuilder,
    private clienteService: ClienteService,
    private route: Router,
    private snackBar: MatSnackBar,
    private aRoute: ActivatedRoute) {

    this.myForm = this.fb.group({
      nombres: ['', [Validators.required]],
      apellidos: ['', [Validators.required]],
      nombre_emprendimiento: ['', [Validators.required]],
      desc_emprendimiento: ['', [Validators.required]],
      correo_electronico: ['', [Validators.required, Validators.email]],
      fecha_nacimiento: ['', [Validators.required]],
      fecha_creacion: ['', [Validators.required]]
    });

  }
  ngOnInit(): void {
    if (this.id_cliente !== undefined) {
    }
  }

  guardarEmprendedor() {
    const emprendedor: Emprendedor = {
      id: null,
      nombres: this.myForm.get('nombres').value,
      apellidos: this.myForm.get('apellidos').value,
      nombre_emprendimiento: this.myForm.get('nombre_emprendimiento').value,
      desc_emprendimiento: this.myForm.get('desc_emprendimiento').value,
      correo_electronico: this.myForm.get('correo_electronico').value,
      fecha_nacimiento: this.myForm.get('fecha_nacimiento').value,
      fecha_creacion: this.myForm.get('fecha_creacion').value
    };

    console.log(emprendedor);

    this.clienteService.agregarEmprendedor(emprendedor).subscribe(
      (success => this.onAgregarSuccess(success)),
      (error => this.onAgregarError(error))
    );
  }

  onAgregarSuccess(success){
    this.snackBar.open('El cliente fue registrado con exito!', '', {
      duration: 3000,
    });
    this.route.navigate(['/']);
  }

  onAgregarError(error){
    this.snackBar.open('El cliente no pudo ser registrado verifica los campos!', '', {
      duration: 3000,
    });
  }

  onInitiSuccess(success){
    console.log(success);
    this.myForm.patchValue({
      id: success.id,
      nombre: success.nombre,
      email: success.email,
      telefono: success.telefono,
      start_date: success.start_date,
      end_date: success.end_date
    });
  }

  onInitError(erorr){
    alert("error al cargar la data");
  }
}
