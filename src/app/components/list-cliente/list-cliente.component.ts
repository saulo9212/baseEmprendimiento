import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ClienteService } from 'src/app/services/cliente.service';
import { Emprendedor } from 'src/app/models/emprendedor';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-list-cliente',
  templateUrl: './list-cliente.component.html',
  styleUrls: ['./list-cliente.component.css']
})
export class ListClienteComponent implements OnInit {
  displayedColumns: string[] = [ "nombres", "apellidos", "nombre_emprendimiento", "desc_emprendimiento", "correo_electronico", "fecha_nacimiento", "fecha_creacion"];

  dataSource = new MatTableDataSource();
  listEmprendedores: Emprendedor[];

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor(private clienteService: ClienteService, public dialog: MatDialog,
              public snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.cargarEmprendedores();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  cargarEmprendedores() {
    this.clienteService.getEmprendedores().subscribe(
      (success => this.onInitSuccess(success)),
      (error => this.onInitError(error))
    );
  }

  onInitSuccess(success){
    this.listEmprendedores = success;
    this.dataSource = new MatTableDataSource(this.listEmprendedores);
    this.dataSource.paginator  = this.paginator;
    this.dataSource.sort = this.sort;
  }

  onInitError(erorr){
    alert("No hay emprendedores por mostrar");
  }
}
