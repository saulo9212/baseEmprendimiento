import { environment } from './../../environments/environment';
import { Emprendedor } from '../models/emprendedor';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {
  listCliente: Emprendedor[] = [];

  constructor(
    private http: HttpClient
  ) {

  }

  getEmprendedores() :Observable<Emprendedor[]>{
    let header = new HttpHeaders().set('Tipe-content', 'aplication/json')
    const sellersUrl = `${environment.apiUrl}emprendedores`;
    return this.http.get<Emprendedor[]>(sellersUrl, {
      headers: header
    });
  }


  agregarEmprendedor(emprendedor: Emprendedor) : Observable<Emprendedor>{
    const sellersUrl = `${environment.apiUrl}emprendedores`;
    return this.http.post<Emprendedor>(sellersUrl, emprendedor);
  }

  getEmprendedor(index: number): Observable<Emprendedor>{
    const sellersUrl = `${environment.apiUrl}emprendedores/${index}`;
    return this.http.get<Emprendedor>(sellersUrl)
  }
}
