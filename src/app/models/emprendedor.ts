export class Emprendedor {
    id: number;
    nombres: string;
    apellidos: string;
    nombre_emprendimiento: string;
    desc_emprendimiento: string;
    correo_electronico: string;
    fecha_nacimiento: Date;
    fecha_creacion: Date;
}
